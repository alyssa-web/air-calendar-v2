import os
import requests
import json

BASE_URL = f"https://api.airtable.com/v0/{os.getenv('AIRTABLE_BASE_CASHFLOW')}"

headers = { 
    "Authorization": f"Bearer {os.getenv('AIRTABLE_API_KEY')}",
    # "Content-Type": "application/json"
}

def handler(event={}, context={}):
    """ ... """
    # print(event)
    print(event["queryStringParameters"]["start"])
    print(event["queryStringParameters"]["end"])
    # print(context)

    # https://codepen.io/airtable/full/rLKkYB?baseId=appHJBdRL96s2xZTU&tableId=tbl22yoXC2MPFaxUn
    url = f"{BASE_URL}/Transactions?fields%5B%5D=Name&fields%5B%5D=Date&filterByFormula=AND(IS_AFTER(%7BDate%7D%2C%22{event['queryStringParameters']['start']}%22),IS_BEFORE(%7BDate%7D%2C%22{event['queryStringParameters']['end']}%22))&maxRecords=1000"

    # Make an API call and store the response.
    r = requests.get(url, headers=headers)
    print(url)

    # Store API response in a variable.
    response_dict = r.json()

    # print(f"Got {len(response_dict['records'])} records(s)\n")

    body = [
        { # https://fullcalendar.io/docs/event-object
            'id': transaction['id'], 
            'title': transaction['fields']['Name'], 
            'start': transaction['fields']['Date'] 
        } for transaction in response_dict['records']
    ]

    body.append({
        'title': 'Statement Closing',
        'rrule': {
            'freq': 'monthly',
            'interval': 1,
            'bymonthday': 10,
        }
    })

    # print(body)

    response = {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": True,
            "Access-Control-Allow-Methods": "OPTIONS,GET"
        },
        "body": json.dumps(body)
    }

    return response